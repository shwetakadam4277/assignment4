package com.nagarro.dao;

import java.util.List;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.hibernate.SessionFactory;

import com.nagarro.model.TShirt;

@Repository
public class TShirtDao {
	public List<com.nagarro.model.TShirt> getAllTShirts() {

		final SessionFactory factory = HibernateUtil.getSessionFactory();

		String hql = "From TShirt";
		Session session = factory.openSession();
		Query query = session.createQuery(hql);
		List<TShirt> matchedTShirts = query.list();
		return matchedTShirts;
	}
}
